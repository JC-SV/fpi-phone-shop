// Conveniently import this file anywhere to use db

import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/storage';

export const db = firebase
  .initializeApp({ projectId: 'fpi-phones', storageBucket: 'fpi-phones.appspot.com' })
  .firestore();
export const storage = firebase.storage();
// Export types that exists in Firestore - Uncomment if you need them in your app
// const { Timestamp, GeoPoint } = firebase.firestore
// export { Timestamp, GeoPoint }
