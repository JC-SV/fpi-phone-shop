import { db } from '../db';
import { message } from '../toaster';

export const postAnuncio = async (anuncio) => {
  console.log('reached');
  try {
    db.collection('anuncios').add(anuncio);
    message.success('Anuncio publicado con exito!');
  } catch (error) {
    message.error('Error al publicar anuncio.');
  }
};
