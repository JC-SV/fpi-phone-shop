import Vue from 'vue';
import { BVToastPlugin } from 'bootstrap-vue';

Vue.use(BVToastPlugin);
const context = new Vue({}).$bvToast;

export const message = {
  success: function (message) {
    context.toast(message, {
      variant: 'success',
      noCloseButton: true,
      solid: true,
      autoHideDelay: 1000,
    });
  },

  error: function (message) {
    context.toast(message, {
      variant: 'danger',
      noCloseButton: true,
      solid: true,
      autoHideDelay: 1000,
    });
  },

  info: function (message) {
    context.toast(message, {
      variant: 'info',
      noCloseButton: true,
      solid: true,
      autoHideDelay: 1000,
    });
  },

  warning: function (message) {
    context.toast(message, {
      variant: 'warning',
      noCloseButton: true,
      solid: true,
      autoHideDelay: 1000,
    });
  },
};
